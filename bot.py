
import irclib
import ircbot
 
class Bot(ircbot.SingleServerIRCBot):
    def __init__(self):
        ircbot.SingleServerIRCBot.__init__(self, [("irc.freenode.com", 6667)],
                                           "BotIRC", "Je suis un bot de bar.")
    def on_welcome(self, serv, ev):
        serv.join("#test-irc")
    def on_pubmsg(self, serv, ev):
        message = ev.arguments()[0]
        if "!botirc" in message or "!botIRC" in message:
            serv.privmsg("#test-irc", "et "+message[8:100] + " !")
 
if __name__ == "__main__":
    Bot().start()
